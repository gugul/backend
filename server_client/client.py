import socket as s

HOST = "127.0.0.1"
PORT = 49357

HEADER = 8
# BUFFER = 1024

client_socket = s.socket(s.AF_INET, s.SOCK_STREAM)
client_socket.connect((HOST, PORT))

name = input("Twoje imie: ").encode("utf8")

name_header = f"{len(name):<{HEADER}}".encode("utf8")

client_socket.send(name_header + name)

response_header = client_socket.recv(HEADER).decode("utf8")
response_length = int(response_header.strip())
uptime_response = client_socket.recv(response_length).decode("utf8")
print(f"Czas działania serwera: {uptime_response}")


