import socket as s
import time

HOST = "127.0.0.1"
PORT = 49357

HEADER = 8
server_start_time = time.time()
# BUFFER = 1024

server_socket = s.socket(s.AF_INET, s.SOCK_STREAM)
server_socket.bind((HOST, PORT))
server_socket.listen(2)


def get_uptime():
    current_time = time.time()
    uptime = current_time - server_start_time
    return str(int(uptime))


while True:
    client_socket, address = server_socket.accept()

    print(f"Uzyskano połączenie od {address} | lub {address[0]}:{address[1]}")

    name_header = client_socket.recv(HEADER).decode("utf8")
    header_len = int(name_header)
    name = client_socket.recv(header_len).decode("utf8")

    print(f"[{address[0]}:{address[1]}]> Nazwa użytkownika: {name}")

    command_header = client_socket.recv(HEADER).decode("utf8")
    if command_header:
        command_len = int(command_header)
        command = client_socket.recv(command_len).decode("utf8")

        if command == "uptime":
            uptime = get_uptime()
            client_socket.send(uptime.encode("utf8"))
