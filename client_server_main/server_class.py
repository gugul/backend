import socket
from datetime import datetime
import json


class Server:
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.version = "Version 1.0.0"
        self.commands = {
            "uptime": "Server uptime",
            "info": "Server version",
            "stop": "Closing connection",
            "help": "Commands list"
        }

    def connect(self):
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.bind((self.host, self.port))
        self.server_start_time = datetime.now()
        self.server_socket.listen()
        print(f"Server started at {self.host}:{self.port}. Waiting for connection...")
        self.client_socket, self.address = self.server_socket.accept()
        print(f"Connection from {self.address} has been established.")

    def receive_request(self):
        return self.client_socket.recv(1024).decode("utf-8")

    def client_command_answer(self, request):

        if request == "uptime":
            now = datetime.now()
            server_uptime = now - self.server_start_time
            return json.dumps(f"Server uptime: {str(server_uptime)}")

        elif request == "stop":
            return json.dumps("Closing connection")

        elif request == "help":
            commands_info = "\n".join([f"{command}:   {description}" for command, description in self.commands.items()])
            return json.dumps(commands_info)

        elif request == "info":
            return json.dumps(self.version)

        else:
            return json.dumps(f"Unknown command: {request}")

    def send_response_to_client(self, response):
        self.client_socket.send(response.encode("utf-8"))


HOST = "127.0.0.1"
PORT = 49357
server = Server(HOST, PORT)
server.connect()

while True:
    request = server.receive_request()
    response = server.client_command_answer(request)
    server.send_response_to_client(response)

    if request == "stop":
        server.server_socket.close()
        print("Closing server")
        break
