import socket
import json


class Client:
    def __init__(self, host, port):
        self.host = host
        self.port = port

    def connect(self):
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client_socket.connect((self.host, self.port))

    def send_request(self, request):
        self.client_socket.send(request.encode("utf-8"))

    def receive_response(self):
        return json.loads(self.client_socket.recv(1024).decode("utf-8"))


HOST = "127.0.0.1"
PORT = 49357

client = Client(HOST, PORT)
client.connect()

while True:
    client_request = input("Enter command: ")
    client.send_request(client_request)
    print(client.receive_response())

    if client_request == "stop":
        client.client_socket.close()
        break
