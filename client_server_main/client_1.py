import socket
import json

HOST = "127.0.0.1"
PORT = 49357

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect((HOST, PORT))

while True:
    client_request = input("Enter command: ")
    client_socket.send(client_request.encode("utf-8"))
    response = json.loads(client_socket.recv(1024).decode("utf-8"))
    print(response)

    if client_request == "stop":
        client_socket.close()
        break


