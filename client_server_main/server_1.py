import socket
from datetime import datetime
import json

HOST = "127.0.0.1"
PORT = 49357
server_ver = "Version: 1.0.0"
commands = {
    "uptime": "Server uptime",
    "info": "Server version",
    "stop": "Closing connection",
    "help": "Commands list"
}


server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind((HOST, PORT))
server_socket.listen()


while True:
    client_socket, address = server_socket.accept()
    print(f"Connection from {address} has been established.")
    server_start_time = datetime.now()

    while True:
        try:
            request = client_socket.recv(1024).decode("utf-8")
            if not request:
                break

            if request == "uptime":
                now = datetime.now()
                server_uptime = now - server_start_time
                answer = json.dumps(f"Server uptime: {str(server_uptime)}")
                client_socket.send(answer.encode("utf-8"))

            elif request == "stop":
                answer = json.dumps("Closing connection")
                client_socket.send(answer.encode("utf-8"))
                break

            elif request == "help":
                commands_info = "\n".join([f"{command}:   {description}" for command, description in commands.items()])
                answer = json.dumps(commands_info)
                client_socket.send(answer.encode("utf-8"))

            elif request == "info":
                answer = json.dumps(server_ver)
                client_socket.send(answer.encode("utf-8"))

            else:
                answer = json.dumps(f"Unknown command: {request}")
                client_socket.send(answer.encode("utf-8"))

        except ConnectionResetError:
            print("Client disconnected unexpectedly.")
            break

    if client_socket:
        client_socket.close()
    print("Ready to accept a new connection.")

